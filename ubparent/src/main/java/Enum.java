import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.iterators.FilterIterator;

/**
 * @author tve Actual enumerations should be singletons, with public final elements. This class is meant to be inherited from by those enumerations. An enumeration is a set that is immutable once it
 * is built.
 */
public class Enum<E> {

   private final Set<List<E>> _elements;

   protected Enum() {
      this(true);
   }

   protected Enum(boolean sorted) {
      _elements = createElements(sorted);
   }

   protected Set<List<E>> createElements(boolean sorted) {
      if (sorted) {
         return new TreeSet<List<E>>();
      }

      // use insertion order
      return new LinkedHashSet<>();
   }

   protected void add(List<E> element) {
      if (element != null) {
         if (_elements.contains(element)) {
            throw new RuntimeException(element + " already exists in the element enumeration!");
         }

         _elements.add(element);
      }
   }

   public boolean contains(List<E> element) {
      if (element == null) {
         return false;
      } else {
         return _elements.contains(element);
      }
   }

   /**
    * @return the values in increasing order.
    */
   public Iterator<List<E>> iterator() {
      return _elements.iterator();
   }

   public int size() {
      return _elements.size();
   }

   @SuppressWarnings("unchecked")
   private List<E> findElement(Predicate predicate) {
      FilterIterator it = new FilterIterator(iterator(), predicate);
      List<E> result = null;

      if (it.hasNext()) {
         result = (List<E>) it.next();
      }

      return result;
   }

}
